package com.example.sesion05tarea06.model

import java.io.Serializable

data class Contacto (
    val nombre:String,
    val correo:String,
    val cargo:String,
    val numero:String
) : Serializable
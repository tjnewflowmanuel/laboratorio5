package com.example.sesion05tarea06

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.sesion05tarea06.model.Contacto

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destino)

        //Bundle
        val bundle:Bundle?= intent.extras
        bundle?.let {
            val contacto = it.getSerializable("KEY_CONTACTO") as Contacto
            println("${contacto.nombre}")
            println("${contacto.cargo}")
            println("${contacto.correo}")
            println("${contacto.numero}")


        }

    }
}
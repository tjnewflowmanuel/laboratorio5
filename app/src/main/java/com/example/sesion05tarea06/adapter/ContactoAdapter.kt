package com.example.sesion05tarea06.adapter

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.sesion05tarea06.MainActivity
import com.example.sesion05tarea06.R
import com.example.sesion05tarea06.model.Contacto
import kotlinx.android.synthetic.main.item_contacto.view.*
import java.util.jar.Manifest

class ContactoAdapter(var contactos: MutableList<Contacto>,
                      val contexto:Context,
                      val itemCallback : (item:Contacto)-> Unit):RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>() {

    class ContactoAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contacto: Contacto, contexto:Context) {
            itemView.textView2.text = contacto.nombre.get(0).toString()
            itemView.textView4.text = contacto.nombre
            itemView.textView5.text = contacto.cargo
            itemView.textView6.text = contacto.correo
            //P2-12 Clic en imagen, itemview es el xml, en el holder ya no el evento ONCLIC
            itemView.img1.setOnClickListener {
                println("Hola")
                val intent = Intent(Intent.ACTION_DIAL);
                intent.data = Uri.parse("tel:${contacto.numero}")
                contexto.startActivity(intent)

            }
        }

    }

    //Implementacion de la clase adapter
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_contacto, parent, false)
        return ContactoAdapterViewHolder(view)

    }

    override fun getItemCount(): Int {
        return contactos.size
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {
        val contacto = contactos[position]
        holder.bind(contacto,contexto)
    }
}

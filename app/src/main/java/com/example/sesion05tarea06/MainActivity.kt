package com.example.sesion05tarea06

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sesion05tarea06.adapter.ContactoAdapter
import com.example.sesion05tarea06.model.Contacto
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //lista
    val contactos = mutableListOf<Contacto>()
    //adaptador
    lateinit var adaptador: ContactoAdapter// la variable la inicializara mas adelante
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cargarinformacion()
        configuraradaptador()
    }
    //configurar adaptador
    private fun configuraradaptador() {
        adaptador= ContactoAdapter(contactos,this) {
            Toast.makeText(this,it.nombre,Toast.LENGTH_SHORT).show()
            val bundle=Bundle().apply {

                putSerializable("KEY_CONTACTO",it)
                /**  putString("KEY NOMBRE",it.nombre)
                putString("KEY CORREO",it.correo)
                putString("KEY CARGO",it.cargo)
                putString("KEY NUMERO",it.numero)**/

            }
            val intent = Intent(this,DestinoActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
        recycleviewcontactos.adapter=adaptador

        recycleviewcontactos.layoutManager=LinearLayoutManager(this)
    }

    //clase Contacto
    private fun cargarinformacion() {
        contactos.add(Contacto("Edilberto","tjnewflowmanuel@gmail.com","diseñador","999876555"))
        contactos.add(Contacto("Manuel","pokemonxyz@gmail.com","analista ","054444666"))
        contactos.add(Contacto("Raul","manuel@gmail.com","tecnico","054987654"))
        contactos.add(Contacto("Paul","paulxyz@gmail.com","contador","999876444"))
        contactos.add(Contacto("Diego","diegitoyz@gmail.com","cargador frontal","999876333"))
        contactos.add(Contacto("Rudecindo","rudersl@gmail.com","mecanico","999876222"))
    }

}